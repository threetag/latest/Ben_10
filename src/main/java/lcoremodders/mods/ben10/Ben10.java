package lcoremodders.mods.ben10;

import lcoremodders.mods.ben10.aliens.Ben10Aliens;
import lcoremodders.mods.ben10.network.PacketDispatcher;
import lcoremodders.mods.ben10.proxy.CommonProxy;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import org.apache.logging.log4j.Logger;

@Mod(modid = Ben10.MODID, version = Ben10.VERSION, name = Ben10.NAME)
public class Ben10 {

	public static final String MODID = "lcoreben10";
	public static final String NAME = "Ben 10";
	public static final String VERSION = "Beta-1.0.0";

	@SidedProxy(clientSide = "lcoremodders.mods.ben10.proxy.ClientProxy", serverSide = "lcoremodders.mods.ben10.proxy.CommonProxy")
	public static CommonProxy proxy;

	@Mod.Instance
	public static Ben10 instance;

	public static Logger LOGGER;

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent e) {
		LOGGER = e.getModLog();

		proxy.preInit(e);

		Ben10Aliens.register();
		NetworkRegistry.INSTANCE.registerGuiHandler(instance, new Ben10GuiHandler());
	}

	@Mod.EventHandler
	public void init(FMLInitializationEvent e) {
		PacketDispatcher.registerMessages();
		proxy.init(e);
		MinecraftForge.EVENT_BUS.register(new Ben10CommonEvents());
	}
}