package lcoremodders.mods.ben10.abilities.conditions;

import lcoremodders.mods.ben10.aliens.Alien;
import lcoremodders.mods.ben10.aliens.AlienHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityCondition;
import net.minecraft.util.text.TextComponentTranslation;

public class AbilityConditionAlien extends AbilityCondition {

	public AbilityConditionAlien(Alien alien) {
		super((a) -> (AlienHandler.getAlien(a.getEntity()) == alien), new TextComponentTranslation("lcoreben10.ability.condition.alien", alien.getDisplayName()));
	}
}
