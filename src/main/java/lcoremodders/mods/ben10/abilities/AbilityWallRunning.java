package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataBoolean;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataFloat;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.util.events.RenderModelEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class AbilityWallRunning extends AbilityToggle {

	public static final AbilityData<Float> SPEED = new AbilityDataFloat("speed").disableSaving().setSyncType(EnumSync.SELF).enableSetting("speed", "The minimum speed required to wall run.");
	public static final AbilityData<Boolean> WALL_RUNNING = new AbilityDataBoolean("wall_running").setSyncType(EnumSync.SELF);

	public AbilityWallRunning(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();

		this.dataManager.register(SPEED, 1f);
		this.dataManager.register(WALL_RUNNING, false);
	}

	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 9);
	}

	@Override
	public void updateTick() {
		if (entity.collidedHorizontally && AbilitySuperSpeed.getSpeed(entity) >= getDataManager().get(SPEED)) {
			entity.motionY = 0.8D;
			entity.fallDistance = 0f;
			getDataManager().set(WALL_RUNNING, true);
		} else if (getDataManager().get(WALL_RUNNING)) {
			getDataManager().set(WALL_RUNNING, false);
			entity.motionY = 0.2d;
			entity.fallDistance = 0f;
		}
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.setDataValue(WALL_RUNNING, enabled);
	}

	@SideOnly(Side.CLIENT)
	@Mod.EventBusSubscriber(modid = Ben10.MODID, value = Side.CLIENT)
	public static class Renderer {

		@SubscribeEvent
		public static void onRenderModel(RenderModelEvent e) {
			if(e.getEntityLiving() instanceof EntityPlayer) {
				for (AbilityWallRunning ab : Ability.getAbilitiesFromClass(Ability.getAbilities(e.getEntityLiving()), AbilityWallRunning.class)) {
					if(ab != null && ab.isUnlocked() && ab.getDataManager() != null && ab.getDataManager().get(WALL_RUNNING)) {
						GlStateManager.translate(0, -e.getEntity().height, 0);
						GlStateManager.rotate(-90, 1, 0, 0);
						break;
					}
				}
			}
		}

		@SubscribeEvent
		public static void setupModel(RenderModelEvent.SetRotationAngels e) {
			if(e.getEntity() instanceof EntityPlayer) {
				for (AbilityWallRunning ab : Ability.getAbilitiesFromClass(Ability.getAbilities((EntityLivingBase) e.getEntity()), AbilityWallRunning.class)) {
					if(ab != null && ab.isUnlocked() && ab.getDataManager().get(WALL_RUNNING)) {
						e.limbSwing = (float) e.getEntity().posY;
						e.limbSwingAmount = MathHelper.cos((float) e.getEntity().posY * 4F);
						break;
					}
				}
			}
		}

	}

}