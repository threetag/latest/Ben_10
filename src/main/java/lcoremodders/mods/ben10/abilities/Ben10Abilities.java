package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionAlien;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionIsTransformed;
import lcoremodders.mods.ben10.aliens.Alien;
import lcoremodders.mods.ben10.aliens.AlienHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityEntry;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityCondition;
import net.minecraft.util.JsonUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = Ben10.MODID)
public class Ben10Abilities {

	@SubscribeEvent
	public static void registerAbilities(RegistryEvent.Register<AbilityEntry> e) {
		e.getRegistry().register(new AbilityEntry(AbilityOmnitrix.class, new ResourceLocation(Ben10.MODID, "omnitrix")));
		e.getRegistry().register(new AbilityEntry(AbilitySelectionMode.class, new ResourceLocation(Ben10.MODID, "selection_mode")));
		e.getRegistry().register(new AbilityEntry(AbilityAlienTransform.class, new ResourceLocation(Ben10.MODID, "alien_transform")));
		e.getRegistry().register(new AbilityEntry(AbilityChangeAlien.class, new ResourceLocation(Ben10.MODID, "change_alien")));

		e.getRegistry().register(new AbilityEntry(AbilitySuperJump.class, new ResourceLocation(Ben10.MODID, "super_jump")));
		e.getRegistry().register(new AbilityEntry(AbilitySuperSpeed.class, new ResourceLocation(Ben10.MODID, "super_speed")));
		e.getRegistry().register(new AbilityEntry(AbilityChangeSpeed.class, new ResourceLocation(Ben10.MODID, "change_speed")));
		e.getRegistry().register(new AbilityEntry(AbilityWallRunning.class, new ResourceLocation(Ben10.MODID, "wall_running")));
		e.getRegistry().register(new AbilityEntry(AbilityBlast.class, new ResourceLocation(Ben10.MODID, "blast")));
		e.getRegistry().register(new AbilityEntry(AbilityTornado.class, new ResourceLocation(Ben10.MODID, "tornado")));
	}

	@SubscribeEvent
	public static void registerConditions(RegistryEvent.Register<AbilityCondition.ConditionEntry> e) {
		AbilityCondition.ConditionEntry.register(e.getRegistry(), AbilityConditionIsTransformed.class, new ResourceLocation(Ben10.MODID, "is_transformed"), ((json, ability, abilities) -> {
			Ability ab = abilities.get(JsonUtils.getString(json, "ability"));
			return (ab != null && ab instanceof AbilityOmnitrix) ? new AbilityConditionIsTransformed((AbilityOmnitrix) ab) : null;
		}));

		AbilityCondition.ConditionEntry.register(e.getRegistry(), AbilityConditionAlien.class, new ResourceLocation(Ben10.MODID, "alien"), ((json, ability, abilities) -> {
			Alien alien = AlienHandler.getAlien(new ResourceLocation(JsonUtils.getString(json, "alien")));
			return alien != null ? new AbilityConditionAlien(alien) : null;
		}));
	}
}
