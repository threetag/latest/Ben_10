package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;

public class AbilityAlienTransform extends AbilityToggle {

	public AbilityAlienTransform(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 2);
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (enabled) {
			for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
				ab.getDataManager().set(AbilityOmnitrix.IS_TRANSFORMING, true);
				ab.setTransformationTimer(0);
			}
		} else {
			for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
				ab.getDataManager().set(AbilityOmnitrix.IS_TRANSFORMED, false);
			}
		}
		super.setEnabled(enabled);
	}

	@Override
	public void updateTick() {
		if (isEnabled()) {
			for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
				if (!ab.getDataManager().get(AbilityOmnitrix.IS_TRANSFORMING) && ab.getTransformationTimer() == 0) {
					ab.getDataManager().set(AbilityOmnitrix.IS_TRANSFORMED, true);
				}
			}
		}
	}
}