package lcoremodders.mods.ben10.abilities;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.data.AbilityDataSuperSpeedHUDPosition;
import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityToggle;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityData;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataColor;
import lucraft.mods.lucraftcore.superpowers.abilities.data.AbilityDataInteger;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityCondition;
import lucraft.mods.lucraftcore.superpowers.abilities.supplier.EnumSync;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IStringSerializable;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;
import java.util.UUID;

@Mod.EventBusSubscriber
public class AbilitySuperSpeed extends AbilityToggle {

	public static final AbilityData<Integer> SPEED_LEVELS = new AbilityDataInteger("speed_levels").setSyncType(EnumSync.SELF).disableSaving().enableSetting("speed_levels", "The number of speed levels determine the max speed.");
	public static final AbilityData<Integer> SPEED_LEVEL = new AbilityDataInteger("speed_level").setSyncType(EnumSync.SELF);
	public static final AbilityData<Position> HUD_POSITION = new AbilityDataSuperSpeedHUDPosition("hud_position").disableSaving().enableSetting("hud_position", "Sets the position for the speed HUD. Available positions: RIGHT, CENTER, LEFT");
	public static final AbilityData<Color> COLOR = new AbilityDataColor("color").disableSaving().enableSetting("color", "Sets the color for the hud.");

	public static final UUID uuid = UUID.fromString("5ad4d81a-2117-49e2-9402-45f83774a189");

	public AbilitySuperSpeed(EntityLivingBase entity) {
		super(entity);
	}

	@Override
	public void registerData() {
		super.registerData();
		this.dataManager.register(SPEED_LEVELS, 10);
		this.dataManager.register(SPEED_LEVEL, 1);
		this.dataManager.register(HUD_POSITION, Position.CENTER);
		this.dataManager.register(COLOR, new Color(80, 168, 185));
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void drawIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawIcon(mc, gui, x, y, 0, 6);
	}

	@Override
	public void updateTick() {
		if (getSpeedLevel() > getMaxSpeedLevel()) {
			setSpeedLevel(getMaxSpeedLevel());
			updateAttribute();
		}

		if (isEnabled()) {
			if (this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).getModifier(uuid) == null
					|| this.entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED).getModifier(uuid) == null) {
				resetAttribute();
				updateAttribute();
			}
		}
	}

	@Override
	public void lastTick() {
		super.lastTick();
		resetAttribute();
	}

	@Override
	public void setEnabled(boolean enabled) {
		if (isEnabled() != enabled) {
			if (!enabled) {
				this.resetAttribute();
			} else {
				this.setSpeedLevel(1);
				this.updateAttribute();
			}
		}
		super.setEnabled(enabled);
	}

	public int getSpeedLevel() {
		return this.dataManager.get(SPEED_LEVEL);
	}

	public int getMaxSpeedLevel() {
		return this.dataManager.get(SPEED_LEVELS);
	}

	public void setSpeedLevel(int speedLevel) {
		speedLevel = MathHelper.clamp(speedLevel, 1, getMaxSpeedLevel());
		this.dataManager.set(SPEED_LEVEL, speedLevel);
	}

	public void updateAttribute() {
		resetAttribute();

		this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED)
				.applyModifier(new AttributeModifier(uuid, Ben10.MODID + ".speed", getSpeedLevel(), 2));
		this.entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED)
				.applyModifier(new AttributeModifier(uuid, Ben10.MODID + ".attackspeed", getSpeedLevel(), 2));
	}

	public void resetAttribute() {
		this.entity.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(uuid);
		this.entity.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED).removeModifier(uuid);
	}

	public static Double getSpeed(Entity entity) {
		return Math.abs(new Vec3d(entity.posX, entity.posY, entity.posZ).distanceTo(new Vec3d(entity.prevPosX, entity.prevPosY, entity.prevPosZ))) * 72D;
	}

	@SubscribeEvent
	public static void onPlayerLoggedIn(EntityJoinWorldEvent e) {
		if (e.getEntity() instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) e.getEntity();
			player.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).removeModifier(AbilitySuperSpeed.uuid);
			player.getEntityAttribute(SharedMonsterAttributes.ATTACK_SPEED).removeModifier(AbilitySuperSpeed.uuid);
		}
	}

	@Mod.EventBusSubscriber(modid = Ben10.MODID, value = Side.CLIENT)
	public static class Renderer {
		private static Minecraft mc = Minecraft.getMinecraft();
		private static ResourceLocation HUD_TEX = new ResourceLocation(Ben10.MODID, "textures/gui/speed_hud.png");

		@SubscribeEvent
		public static void renderGameOverlay(RenderGameOverlayEvent e) {
			if (e.isCancelable() || e.getType() != RenderGameOverlayEvent.ElementType.EXPERIENCE) {
				return;
			}

			AbilitySuperSpeed ability = null;

			for (AbilitySuperSpeed ab : Ability.getAbilitiesFromClass(Ability.getAbilities(mc.player), AbilitySuperSpeed.class)) {
				ability = ab;
				break;
			}

			if (ability == null || !ability.isEnabled() || mc.gameSettings.hideGUI || mc.gameSettings.showDebugInfo || mc.player == null)
				return;

			for (AbilityCondition condition : ability.getConditions()) {
				if (!condition.test(ability)) {
					return;
				}
			}

			Position position = ability.getDataManager().get(AbilitySuperSpeed.HUD_POSITION);

			GlStateManager.pushMatrix();
			GlStateManager.color(1, 1, 1, 1);
			mc.renderEngine.bindTexture(HUD_TEX);
			int maxSpeedLevel = ability.getMaxSpeedLevel();
			int speedLevel = ability.getSpeedLevel();
			int barHeight = (position == Position.LEFT || position == Position.RIGHT) ? 9 + (maxSpeedLevel - 1) * 7 : 9;
			int startY = (position == Position.LEFT || position == Position.RIGHT) ? e.getResolution().getScaledHeight() / 2 - barHeight / 2 : (e.getResolution().getScaledHeight() / 4) * 3;
			int startX = (position == Position.RIGHT) ? e.getResolution().getScaledWidth() - 15 : (position == Position.CENTER) ? e.getResolution().getScaledWidth()/2 - (9 + (maxSpeedLevel - 1) * 7)/2 : 0;

			GlStateManager.translate(startX, startY, 0);
			for (int i = 0; i < maxSpeedLevel; i++) {
				// Border
				if (position == Position.RIGHT || position == Position.LEFT) {
					mc.ingameGUI.drawTexturedModalRect(0, i * 7, 0, 0, 15, 9);
				} else {
					mc.ingameGUI.drawTexturedModalRect(i * 7, 0, 0, 15, 9, 15);
				}

				Color color = ability.getDataManager().get(AbilitySuperSpeed.COLOR);
				GlStateManager.color(color.getRed()/255F, color.getGreen()/255F, color.getBlue()/255F, 1.0F);

				// Slot
				if (position == Position.RIGHT) {
					mc.ingameGUI.drawTexturedModalRect(1, i * 7 + 1, 0, 9, 12, 6);
				} else if (position == Position.LEFT) {
					mc.ingameGUI.drawTexturedModalRect(1, i * 7 + 1, 0, 9, 12, 6);
				} else if (position == Position.CENTER) {
					mc.ingameGUI.drawTexturedModalRect(i * 7 + 1, 1, 9, 15, 6, 12);
				}
				GlStateManager.color(1, 1, 1, 1);
			}

			// Speed Cursor
			if (position == Position.RIGHT) {
				mc.ingameGUI.drawTexturedModalRect(-5, (maxSpeedLevel - speedLevel) * 7 + 1, 15, 0, 4, 7);
			} else if (position == Position.LEFT) {
				mc.ingameGUI.drawTexturedModalRect(16, (maxSpeedLevel - speedLevel) * 7 + 1, 19, 0, 4, 7);
			} else if (position == Position.CENTER) {
				mc.ingameGUI.drawTexturedModalRect((speedLevel-1) * 7 + 1, -5, 23, 0, 7, 4);
			}
			GlStateManager.popMatrix();

			// Speed Text
			double d = round(AbilitySuperSpeed.getSpeed(mc.player), 2);
			String detail = StringHelper.translateToLocal(Ben10.MODID + ".info.speed") + ": " + (d < 1D ? 0 : d) + "km/h";
			int stringLength = mc.fontRenderer.getStringWidth(detail);
			LCRenderHelper.drawStringWithOutline(detail, e.getResolution().getScaledWidth() / 2 - stringLength / 2,   ((e.getResolution().getScaledHeight() / 4) * 3) - 15, 0xffffff, 0);
		}

		private static double round(double value, int places) {
			if (places < 0)
				throw new IllegalArgumentException();

			long factor = (long) Math.pow(10, places);
			value = value * factor;
			long tmp = Math.round(value);
			return (double) tmp / factor;
		}
	}

	public enum Position implements IStringSerializable {
		RIGHT, CENTER, LEFT;

		@Override
		public String getName() {
			return toString().toLowerCase();
		}

		public static boolean isInside(String value) {
			for (Position pos : values()) {
				if (pos.getName().equalsIgnoreCase(value)) {
					return true;
				}
			}
			return false;
		}

		public static Position getValue(String value) {
			if (isInside(value)) {
				for (Position pos : values()) {
					if (pos.getName().equalsIgnoreCase(value)) {
						return pos;
					}
				}
			}
			return null;
		}
	}
}