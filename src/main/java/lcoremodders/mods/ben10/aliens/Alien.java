package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lcoremodders.mods.ben10.aliens.render.IAlienRenderer;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.util.helper.StringHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistryEntry;

public abstract class Alien extends IForgeRegistryEntry.Impl<Alien> implements INBTSerializable<NBTTagCompound> {

	private String name;
	private boolean hasLights = false;

	public Alien(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return StringHelper.translateToLocal(getModId().toLowerCase() + ".alien." + getName() + ".name");
	}

	public String getModId() {
		return Ben10.MODID;
	}

	public ResourceLocation getAlienTexture(boolean hasLights) {
		return new ResourceLocation(getModId(), "textures/models/alien/" + getName() + "/" + this.getName() + (hasLights ? "_lights.png" : ".png"));
	}

	public void setLights(boolean _new) {
		this.hasLights = _new;
	}

	public boolean hasLights() {
		return hasLights;
	}

	@SideOnly(Side.CLIENT)
	public IAlienRenderer getRenderer() {
		return null;
	}

	@Override
	public NBTTagCompound serializeNBT() {
		return new NBTTagCompound();
	}

	@Override
	public void deserializeNBT(NBTTagCompound nbt) {}

	public abstract Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context, AbilityOmnitrix abilityOmnitrix);
}