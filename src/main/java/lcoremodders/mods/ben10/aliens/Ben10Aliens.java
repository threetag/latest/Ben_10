package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.client.model.ModelFourArms;
import lcoremodders.mods.ben10.client.model.ModelHeatblast;
import lcoremodders.mods.ben10.client.model.ModelXLR8;
import net.minecraft.client.model.ModelBiped;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.HashMap;

@Mod.EventBusSubscriber
public class Ben10Aliens {

	/* OG Aliens */
	public static final Alien FOUR_ARMS = new AlienFourArms();
	public static final Alien HEATBLAST = new AlienHeatblast();
	public static final Alien XLR8 = new AlienXLR8();

	public static void register() {
		AlienHandler.registerAlien(FOUR_ARMS);
		AlienHandler.registerAlien(HEATBLAST);
		AlienHandler.registerAlien(XLR8);

	}

	@SideOnly(Side.CLIENT)
	public static class Client {
		public static HashMap<Class<? extends Alien>, ModelBiped> ALIEN_MODELS = new HashMap<>();

		public static void clientInit() {
			ALIEN_MODELS.put(AlienFourArms.class, new ModelFourArms());
			ALIEN_MODELS.put(AlienHeatblast.class, new ModelHeatblast());
			ALIEN_MODELS.put(AlienXLR8.class, new ModelXLR8());
		}
	}
}