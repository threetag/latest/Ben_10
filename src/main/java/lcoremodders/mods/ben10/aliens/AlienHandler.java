package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;

import java.util.HashMap;

public class AlienHandler {

	public static HashMap<ResourceLocation, Alien> ALIEN_REGISTRY = new HashMap<>();

	public static void registerAlien(Alien alien) {
		ALIEN_REGISTRY.put(alien.getRegistryName(), alien);
	}

	public static Alien getAlien(EntityLivingBase entity) {
		if (getAbilityOmnitrix(entity) != null && getAbilityOmnitrix(entity).isEnabled()) {
			return getAbilityOmnitrix(entity).getAlien(getAbilityOmnitrix(entity).getSelectedAlien());
		}
		return null;
	}

	public static Alien getAlien(ResourceLocation registryName) {
		if (ALIEN_REGISTRY.containsKey(registryName))
			return ALIEN_REGISTRY.get(registryName);
		return null;
	}

	public static boolean isTransformed(EntityLivingBase entity) {
		if (getAbilityOmnitrix(entity) != null && getAbilityOmnitrix(entity).isEnabled()) {
			return getAbilityOmnitrix(entity).getDataManager().get(AbilityOmnitrix.IS_TRANSFORMED);
		}
		return false;
	}

	public static int getAliensSize(EntityLivingBase entity) {
		if (getAbilityOmnitrix(entity) != null) {
			return getAbilityOmnitrix(entity).getDataManager().get(AbilityOmnitrix.ALIEN_LIST).getAliens().size();
		}
		return 0;
	}

	public static AbilityOmnitrix getAbilityOmnitrix(EntityLivingBase entity) {
		for (AbilityOmnitrix ab : Ability.getAbilitiesFromClass(Ability.getAbilities(entity), AbilityOmnitrix.class)) {
			return ab;
		}
		return null;
	}
}