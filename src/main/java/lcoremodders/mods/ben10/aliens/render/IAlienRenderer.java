package lcoremodders.mods.ben10.aliens.render;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.player.EntityPlayer;

public interface IAlienRenderer {

	void render(ModelBiped bipedModel, Minecraft mc, EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float mcScale);
}