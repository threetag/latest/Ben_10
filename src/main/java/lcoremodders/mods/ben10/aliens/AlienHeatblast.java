package lcoremodders.mods.ben10.aliens;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.AbilityBlast;
import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionAlien;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionIsTransformed;
import lcoremodders.mods.ben10.aliens.render.IAlienRenderer;
import lucraft.mods.lucraftcore.superpowers.abilities.*;
import lucraft.mods.lucraftcore.util.helper.LCRenderHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.EnumParticleTypes;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.opengl.GL11;

import java.util.UUID;

@Mod.EventBusSubscriber
public class AlienHeatblast extends Alien {

	public AlienHeatblast() {
		super("heatblast");
		setRegistryName(Ben10.MODID, "heatblast");
	}

	private UUID uuid = UUID.fromString("ff8a7b9e-23ac-4da5-a3cd-e326f7cb9872");

	@Override
	public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context, AbilityOmnitrix abilityOmnitrix) {
		abilities.put("heatblast_health", new AbilityHealth(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 10F).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("heatblast_fire_resistance", new AbilityFireResistance(entity).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("heatblast_flight", new AbilityFlight(entity).setDataValue(AbilityFlight.SPEED, 1F).setDataValue(AbilityFlight.SPRINT_SPEED, 1F).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("heatblast_fall_resistance", new AbilityFallResistance(entity).setDataValue(AbilityAttributeModifier.UUID, uuid).setDataValue(AbilityAttributeModifier.AMOUNT, 0.05f).setDataValue(AbilityAttributeModifier.OPERATION, 1).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));
		abilities.put("heatblast_blast", new AbilityBlast(entity).addCondition(new AbilityConditionIsTransformed(abilityOmnitrix)).addCondition(new AbilityConditionAlien(this)));

		return abilities;
	}

	@SubscribeEvent
	public static void playerTickEvent(TickEvent.PlayerTickEvent e) {
		EntityPlayer player = e.player;

		if (AlienHandler.isTransformed(player) && AlienHandler.getAlien(player) == Ben10Aliens.HEATBLAST) {
			for (AbilityFlight flight : Ability.getAbilitiesFromClass(Ability.getAbilities(player), AbilityFlight.class)) {
				if (flight.isEnabled()) {
					double yPos = player.posY - 0.1D;
					// Right Feet
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ + 0.18D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ + 0.2D, 0.0D, 0.0D, 0.0D);
					// Left Feet
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ - 0.18D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ - 0.2D, 0.0D, 0.0D, 0.0D);

					yPos = player.posY + ((player.moveForward > 0F) ? -0.1D : 0.75D);
					// Right Hand
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ + 0.38D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ + 0.4D, 0.0D, 0.0D, 0.0D);
					// Left Hand
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ - 0.38D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.FLAME, player.posX, yPos, player.posZ - 0.4D, 0.0D, 0.0D, 0.0D);

					yPos = player.posY + ((player.moveForward > 0F) ? 0.5D : 2.3D);
					// Head
					player.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX, yPos, player.posZ - 0.15D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX, yPos, player.posZ + 0.15D, 0.0D, 0.0D, 0.0D);
				} else {
					double yPos = player.posY + 2.3D;
					// Head
					player.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX, yPos, player.posZ - 0.15D, 0.0D, 0.0D, 0.0D);
					player.world.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, player.posX, yPos, player.posZ + 0.15D, 0.0D, 0.0D, 0.0D);
				}
			}
		}
	}

	@Override
	public IAlienRenderer getRenderer() {
		return new HeatblastRenderer();
	}

	@SideOnly(Side.CLIENT)
	public class HeatblastRenderer implements IAlienRenderer {

		// TODO Fire Head for Heatblast
		@Override
		public void render(ModelBiped bipedModel, Minecraft mc, EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float mcScale) {
			GlStateManager.pushMatrix();
			GlStateManager.enableBlend();
			GL11.glBlendFunc(770, 771);
			GL11.glAlphaFunc(516, 0.003921569F);
			GlStateManager.color(1, 1, 1, 1);

			GlStateManager.translate(0, -0.5, 0);
			GlStateManager.translate(0, player.isSneaking() ? 0.2F : 0, 0);

			GlStateManager.disableLighting();
			float brightness = 100F + (float) (1 + Math.sin(Minecraft.getMinecraft().player.ticksExisted / 20F)) * 70F;
			LCRenderHelper.setLightmapTextureCoords(brightness, brightness);
			GlStateManager.pushMatrix();
			float s = 0.5F;

			bipedModel.bipedHead.postRender(0.0625f);

			GlStateManager.translate(0, -0.7D, 0);

			GlStateManager.scale(s, s * 1.5F, s);
			GlStateManager.translate(-0.5F, 0F, -0.4F);
			LCRenderHelper.renderFire(mc, "minecraft:blocks/fire_layer_0");
			GlStateManager.scale(1.1D, 1.1D, 1.1D);
			GlStateManager.translate(-0.05D, -0.06D, 0);
			LCRenderHelper.renderFire(mc, "minecraft:blocks/fire_layer_1");
			GlStateManager.popMatrix();

			LCRenderHelper.restoreLightmapTextureCoords();
			GlStateManager.enableLighting();
			GlStateManager.disableBlend();
			GlStateManager.popMatrix();
		}
	}
}