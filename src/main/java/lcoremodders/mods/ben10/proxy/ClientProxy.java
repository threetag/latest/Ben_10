package lcoremodders.mods.ben10.proxy;

import lcoremodders.mods.ben10.aliens.Ben10Aliens;
import lcoremodders.mods.ben10.client.render.item.ItemRendererOGOmnitrix;
import lcoremodders.mods.ben10.items.Ben10Items;
import lucraft.mods.lucraftcore.extendedinventory.render.ExtendedInventoryItemRendererRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {

	@Override
	public void preInit(FMLPreInitializationEvent e) {
		super.preInit(e);
	}

	@Override
	public void init(FMLInitializationEvent e) {
		super.init(e);
		ExtendedInventoryItemRendererRegistry.registerRenderer(Ben10Items.OMNITRIX_OG, new ItemRendererOGOmnitrix());

		Ben10Aliens.Client.clientInit();
	}
}