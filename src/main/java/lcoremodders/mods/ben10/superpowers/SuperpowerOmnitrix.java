package lcoremodders.mods.ben10.superpowers;

import lcoremodders.mods.ben10.abilities.AbilityAlienTransform;
import lcoremodders.mods.ben10.abilities.AbilityChangeAlien;
import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lcoremodders.mods.ben10.abilities.AbilitySelectionMode;
import lcoremodders.mods.ben10.abilities.conditions.AbilityConditionIsTransformed;
import lcoremodders.mods.ben10.aliens.Alien;
import lcoremodders.mods.ben10.aliens.AlienHandler;
import lcoremodders.mods.ben10.util.Ben10RenderHelper;
import lucraft.mods.lucraftcore.superpowers.Superpower;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityConditionAbility;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityConditionNot;
import lucraft.mods.lucraftcore.superpowers.abilities.predicates.AbilityConditionOr;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.entity.EntityLivingBase;

public class SuperpowerOmnitrix extends Superpower {

	public SuperpowerOmnitrix() {
		super("ogOmnitrix");
		this.setRegistryName("ogOmnitrix");
	}

	@Override
	public void renderIcon(Minecraft mc, Gui gui, int x, int y) {
		Ben10RenderHelper.drawSuperpowerIcon(mc, gui, x, y, 0, 0);
	}

	@Override
	public Ability.AbilityMap addDefaultAbilities(EntityLivingBase entity, Ability.AbilityMap abilities, Ability.EnumAbilityContext context) {
		AbilityOmnitrix abilityOmnitrix = new AbilityOmnitrix(entity);
		abilities.put("omnitrix", abilityOmnitrix);

		Ability abilitySelectionMode = new AbilitySelectionMode(entity).addCondition(new AbilityConditionAbility(abilityOmnitrix));
		abilities.put("selection_mode", abilitySelectionMode.addCondition(new AbilityConditionNot(new AbilityConditionIsTransformed(abilityOmnitrix))));

		abilities.put("alien_transform", new AbilityAlienTransform(entity).addCondition(new AbilityConditionOr(new AbilityConditionAbility(abilitySelectionMode), new AbilityConditionIsTransformed(abilityOmnitrix))));
		abilities.put("next_alien", new AbilityChangeAlien(entity).addCondition(new AbilityConditionAbility(abilitySelectionMode)).addCondition(new AbilityConditionNot(new AbilityConditionIsTransformed(abilityOmnitrix))));
		abilities.put("prev_alien", new AbilityChangeAlien(entity).setDataValue(AbilityChangeAlien.NEXT_ALIEN, false).addCondition(new AbilityConditionAbility(abilitySelectionMode)).addCondition(new AbilityConditionNot(new AbilityConditionIsTransformed(abilityOmnitrix))));

		for (Alien alien : AlienHandler.ALIEN_REGISTRY.values()) {
			alien.addDefaultAbilities(entity, abilities, context, abilityOmnitrix);
		}

		return abilities;
	}
}