package lcoremodders.mods.ben10.client.model;

import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

/**
* ModelPlayer - Either Mojang or a mod author
* Created using Tabula 7.0.1
*/
public class ModelHeatblast extends ModelAlien {
	
	private ModelRenderer left_arm_1;
	private ModelRenderer left_leg_1;
	private ModelRenderer lower_body;
	private ModelRenderer right_leg_1;
	private ModelRenderer right_arm_1;
	private ModelRenderer upper_body;
	private ModelRenderer head;
	private ModelRenderer left_arm_3;
	private ModelRenderer left_thumb;
	private ModelRenderer left_finger_3;
	private ModelRenderer left_arm_2;
	private ModelRenderer left_finger_2;
	private ModelRenderer left_finger_1;
	private ModelRenderer left_leg_2;
	private ModelRenderer left_leg_3;
	private ModelRenderer left_toe_1;
	private ModelRenderer left_toe_2;
	private ModelRenderer left_heel;
	private ModelRenderer right_leg_2;
	private ModelRenderer right_leg_3;
	private ModelRenderer right_toe_1;
	private ModelRenderer right_toe_2;
	private ModelRenderer right_heel;
	private ModelRenderer right_arm_3;
	private ModelRenderer right_thumb;
	private ModelRenderer right_finger_3;
	private ModelRenderer right_arm_2;
	private ModelRenderer right_finger_2;
	private ModelRenderer right_finger_1;
	private ModelRenderer mouth;
	private ModelRenderer left_eye_1;
	private ModelRenderer left_eye_2;
	private ModelRenderer headpiece;
	private ModelRenderer right_eye_1;
	private ModelRenderer right_eye_2;

	public ModelHeatblast() {
		super(64, 64);

		this.head = new ModelRenderer(this, 26, 0);
		this.head.setRotationPoint(0.0F, -5.0F, 0.0F);
		this.head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F);
		this.left_finger_1 = new ModelRenderer(this, 50, 2);
		this.left_finger_1.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.left_finger_1.addBox(-0.6F, 4.3F, -1.9F, 3, 1, 1, 0.0F);
		this.setRotateAngle(left_finger_1, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_eye_1 = new ModelRenderer(this, 36, 9);
		this.right_eye_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_eye_1.addBox(-3.5F, -2.5F, -4.3F, 1, 1, 1, 0.0F);
		this.left_toe_2 = new ModelRenderer(this, 28, 2);
		this.left_toe_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_toe_2.addBox(0.4F, 7.0F, -3.5F, 1, 1, 2, 0.0F);
		this.setRotateAngle(left_toe_2, 0.0F, -0.10471975511965977F, 0.0F);
		this.right_toe_1 = new ModelRenderer(this, 50, 4);
		this.right_toe_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_toe_1.addBox(-1.4F, 7.0F, -3.5F, 1, 1, 2, 0.0F);
		this.setRotateAngle(right_toe_1, 0.0F, 0.10471975511965977F, 0.0F);
		this.right_leg_2 = new ModelRenderer(this, 46, 35);
		this.right_leg_2.setRotationPoint(-0.07F, 7.01F, -0.17F);
		this.right_leg_2.addBox(-1.5F, -1.0F, -1.5F, 3, 9, 3, 0.0F);
		this.setRotateAngle(right_leg_2, 0.10471975511965977F, 0.0F, -0.03490658503988659F);
		this.left_leg_3 = new ModelRenderer(this, 32, 35);
		this.left_leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_leg_3.addBox(-1.0F, 1.0F, -2.0F, 3, 5, 4, 0.0F);
		this.right_heel = new ModelRenderer(this, 56, 5);
		this.right_heel.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_heel.addBox(-1.0F, 7.0F, 1.5F, 2, 1, 1, 0.0F);
		this.left_arm_1 = new ModelRenderer(this, 0, 0);
		this.left_arm_1.setRotationPoint(5.0F, -3.0F, 0.0F);
		this.left_arm_1.addBox(-1.0F, -2.0F, -1.5F, 3, 9, 3, 0.0F);
		this.setRotateAngle(left_arm_1, 0.0F, 0.0F, -0.10000736613927509F);
		this.left_toe_1 = new ModelRenderer(this, 57, 0);
		this.left_toe_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_toe_1.addBox(-1.4F, 7.0F, -3.5F, 1, 1, 2, 0.0F);
		this.setRotateAngle(left_toe_1, 0.0F, 0.10471975511965977F, 0.0F);
		this.right_thumb = new ModelRenderer(this, 18, 11);
		this.right_thumb.setRotationPoint(-0.0F, 9.5F, -0.6F);
		this.right_thumb.addBox(-2.4F, 3.3F, -2.3F, 3, 1, 1, 0.0F);
		this.setRotateAngle(right_thumb, -0.22689280275926282F, 0.0F, 0.0F);
		this.mouth = new ModelRenderer(this, 36, 9);
		this.mouth.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.mouth.addBox(-1.0F, -1.0F, -4.3F, 2, 1, 1, 0.0F);
		this.left_arm_2 = new ModelRenderer(this, 48, 16);
		this.left_arm_2.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.left_arm_2.addBox(-1.0F, -3.0F, -1.5F, 3, 5, 3, 0.0F);
		this.setRotateAngle(left_arm_2, -0.22689280275926282F, 0.0F, 0.0F);
		this.left_eye_1 = new ModelRenderer(this, 36, 9);
		this.left_eye_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_eye_1.addBox(2.5F, -2.5F, -4.3F, 1, 1, 1, 0.0F);
		this.left_arm_3 = new ModelRenderer(this, 44, 24);
		this.left_arm_3.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.left_arm_3.addBox(-1.5F, -2.0F, -2.0F, 4, 7, 4, 0.0F);
		this.setRotateAngle(left_arm_3, -0.22689280275926282F, 0.0F, 0.0F);
		this.left_leg_2 = new ModelRenderer(this, 24, 27);
		this.left_leg_2.setRotationPoint(-0.07F, 7.01F, -0.17F);
		this.left_leg_2.addBox(-1.5F, -1.0F, -1.5F, 3, 9, 3, 0.0F);
		this.setRotateAngle(left_leg_2, 0.10471975511965977F, 0.0F, 0.03490658503988659F);
		this.upper_body = new ModelRenderer(this, 0, 23);
		this.upper_body.setRotationPoint(0.0F, -5.0F, 0.0F);
		this.upper_body.addBox(-4.0F, 0.0F, -2.0F, 8, 9, 4, 0.0F);
		this.left_finger_2 = new ModelRenderer(this, 22, 2);
		this.left_finger_2.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.left_finger_2.addBox(-0.6F, 4.3F, -0.5F, 3, 1, 1, 0.0F);
		this.setRotateAngle(left_finger_2, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_eye_2 = new ModelRenderer(this, 36, 9);
		this.right_eye_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_eye_2.addBox(-3.0F, -3.5F, -4.3F, 2, 1, 1, 0.0F);
		this.left_finger_3 = new ModelRenderer(this, 50, 0);
		this.left_finger_3.setRotationPoint(-0.0F, 9.5F, -0.6F);
		this.left_finger_3.addBox(-0.6F, 4.3F, 0.9F, 3, 1, 1, 0.0F);
		this.setRotateAngle(left_finger_3, -0.22689280275926282F, 0.0F, 0.0F);
		this.left_heel = new ModelRenderer(this, 57, 3);
		this.left_heel.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_heel.addBox(-1.0F, 7.0F, 1.5F, 2, 1, 1, 0.0F);
		this.right_arm_3 = new ModelRenderer(this, 12, 36);
		this.right_arm_3.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.right_arm_3.addBox(-2.5F, -2.0F, -2.0F, 4, 7, 4, 0.0F);
		this.setRotateAngle(right_arm_3, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_arm_1 = new ModelRenderer(this, 36, 16);
		this.right_arm_1.setRotationPoint(-5.0F, -3.0F, 0.0F);
		this.right_arm_1.addBox(-2.0F, -2.0F, -1.5F, 3, 9, 3, 0.0F);
		this.setRotateAngle(right_arm_1, 0.0F, 0.0F, 0.10000736613927509F);
		this.right_arm_2 = new ModelRenderer(this, 0, 44);
		this.right_arm_2.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.right_arm_2.addBox(-2.0F, -3.0F, -1.5F, 3, 5, 3, 0.0F);
		this.setRotateAngle(right_arm_2, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_leg_3 = new ModelRenderer(this, 0, 36);
		this.right_leg_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_leg_3.addBox(-2.0F, 1.0F, -2.0F, 3, 5, 3, 0.0F);
		this.right_finger_2 = new ModelRenderer(this, 0, 21);
		this.right_finger_2.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.right_finger_2.addBox(-2.4F, 4.3F, -0.5F, 3, 1, 1, 0.0F);
		this.setRotateAngle(right_finger_2, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_finger_3 = new ModelRenderer(this, 18, 13);
		this.right_finger_3.setRotationPoint(-0.0F, 9.5F, -0.6F);
		this.right_finger_3.addBox(-2.4F, 4.3F, 0.9F, 3, 1, 1, 0.0F);
		this.setRotateAngle(right_finger_3, -0.22689280275926282F, 0.0F, 0.0F);
		this.right_toe_2 = new ModelRenderer(this, 26, 5);
		this.right_toe_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.right_toe_2.addBox(0.4F, 7.0F, -3.5F, 1, 1, 2, 0.0F);
		this.setRotateAngle(right_toe_2, 0.0F, -0.10471975511965977F, 0.0F);
		this.lower_body = new ModelRenderer(this, 0, 12);
		this.lower_body.setRotationPoint(0.0F, -5.0F, 0.0F);
		this.lower_body.addBox(-3.5F, 9.0F, -2.0F, 7, 5, 4, 0.0F);
		this.left_leg_1 = new ModelRenderer(this, 12, 0);
		this.left_leg_1.setRotationPoint(1.9F, 9.0F, 0.1F);
		this.left_leg_1.addBox(-1.4F, 0.0F, -2.0F, 3, 7, 4, 0.0F);
		this.setRotateAngle(left_leg_1, -0.08726646259971647F, 0.0F, -0.03490658503988659F);
		this.right_leg_1 = new ModelRenderer(this, 22, 16);
		this.right_leg_1.setRotationPoint(-1.8F, 9.0F, 0.1F);
		this.right_leg_1.addBox(-1.6F, 0.0F, -2.0F, 3, 7, 4, 0.0F);
		this.setRotateAngle(right_leg_1, -0.08726646259971647F, 0.0F, 0.03490658503988659F);
		this.right_finger_1 = new ModelRenderer(this, 8, 21);
		this.right_finger_1.setRotationPoint(0.0F, 9.5F, -0.6F);
		this.right_finger_1.addBox(-2.4F, 4.3F, -1.9F, 3, 1, 1, 0.0F);
		this.setRotateAngle(right_finger_1, -0.22689280275926282F, 0.0F, 0.0F);
		this.left_eye_2 = new ModelRenderer(this, 36, 9);
		this.left_eye_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.left_eye_2.addBox(1.0F, -3.5F, -4.3F, 2, 1, 1, 0.0F);
		this.left_thumb = new ModelRenderer(this, 22, 0);
		this.left_thumb.setRotationPoint(-0.0F, 9.5F, -0.6F);
		this.left_thumb.addBox(-0.6F, 3.3F, -2.3F, 3, 1, 1, 0.0F);
		this.setRotateAngle(left_thumb, -0.22689280275926282F, 0.0F, 0.0F);
		this.headpiece = new ModelRenderer(this, 36, 8);
		this.headpiece.setRotationPoint(0.0F, 0.0F, 0.0F);
		this.headpiece.addBox(-1.0F, -7.0F, -4.3F, 2, 3, 1, 0.0F);
		this.left_arm_1.addChild(this.left_finger_1);
		this.head.addChild(this.right_eye_1);
		this.left_leg_2.addChild(this.left_toe_2);
		this.right_leg_2.addChild(this.right_toe_1);
		this.right_leg_1.addChild(this.right_leg_2);
		this.left_leg_2.addChild(this.left_leg_3);
		this.right_leg_2.addChild(this.right_heel);
		this.left_leg_2.addChild(this.left_toe_1);
		this.right_arm_1.addChild(this.right_thumb);
		this.head.addChild(this.mouth);
		this.left_arm_1.addChild(this.left_arm_2);
		this.head.addChild(this.left_eye_1);
		this.left_arm_1.addChild(this.left_arm_3);
		this.left_leg_1.addChild(this.left_leg_2);
		this.left_arm_1.addChild(this.left_finger_2);
		this.head.addChild(this.right_eye_2);
		this.left_arm_1.addChild(this.left_finger_3);
		this.left_leg_2.addChild(this.left_heel);
		this.right_arm_1.addChild(this.right_arm_3);
		this.right_arm_1.addChild(this.right_arm_2);
		this.right_leg_2.addChild(this.right_leg_3);
		this.right_arm_1.addChild(this.right_finger_2);
		this.right_arm_1.addChild(this.right_finger_3);
		this.right_leg_2.addChild(this.right_toe_2);
		this.right_arm_1.addChild(this.right_finger_1);
		this.head.addChild(this.left_eye_2);
		this.left_arm_1.addChild(this.left_thumb);
		this.head.addChild(this.headpiece);
	}

	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		super.render(entityIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

		this.head.render(scale);
		this.left_arm_1.render(scale);
		this.upper_body.render(scale);
		this.right_arm_1.render(scale);
		this.lower_body.render(scale);
		this.left_leg_1.render(scale);
		this.right_leg_1.render(scale);
	}

	@Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) {
		super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor, entityIn);

		copyModelAngle(bipedLeftLeg, left_leg_1, "x", -5.0F);
		copyModelAngle(bipedRightLeg, right_leg_1, "x", -5.0F);

		copyModelAngle(bipedBody, upper_body, "x", 0);
		copyModelAngle(bipedBody, upper_body, "y", 0);
		copyModelAngle(bipedBody, upper_body, "z", 0);
		copyModelAngle(bipedBody, lower_body, "x", 0);
		copyModelAngle(bipedBody, lower_body, "y", 0);
		copyModelAngle(bipedBody, lower_body, "z", 0);

		copyModelAngle(bipedHead, head, "x", 0);
		copyModelAngle(bipedHead, head, "y", 0);
		copyModelAngle(bipedHead, head, "z", 0);

		copyModelAngle(bipedLeftArm, left_arm_1, "x", 0);
		copyModelAngle(bipedLeftArm, left_arm_1, "y", 0);
		copyModelAngle(bipedLeftArm, left_arm_1, "z", -5.73f);

		copyModelAngle(bipedRightArm, right_arm_1, "x", 0);
		copyModelAngle(bipedRightArm, right_arm_1, "y", 0);
		copyModelAngle(bipedRightArm, right_arm_1, "z", 5.73f);
	}

	/**
	* This is a helper function from Tabula to set the rotation of model parts
	*/
	public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
