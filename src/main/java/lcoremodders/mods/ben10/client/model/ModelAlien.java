package lcoremodders.mods.ben10.client.model;

import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.superpowers.abilities.AbilityFlight;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;

public class ModelAlien extends ModelBiped {

	public ModelAlien(int width, int height) {
		this.textureWidth = width;
		this.textureHeight = height;
	}

	public void copyModelAngle(ModelRenderer source, ModelRenderer dest, String type, float angle) {
		float converter = (float) (Math.PI/180);
		if (type.equalsIgnoreCase("x")) {
			dest.rotateAngleX = converter*angle + source.rotateAngleX;
		} else if (type.equalsIgnoreCase("y")) {
			dest.rotateAngleY = converter*angle + source.rotateAngleY;
		} else if (type.equalsIgnoreCase("z")) {
			dest.rotateAngleZ = converter*angle + source.rotateAngleZ;
		}
	}

	@Override
	public void render(Entity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
		this.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale, entityIn);
	}

	@Override
	public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) {
		super.setRotationAngles(limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor, entityIn);

		if (entityIn instanceof EntityLivingBase) {
			for (AbilityFlight abilityFlight : Ability.getAbilitiesFromClass(Ability.getAbilities((EntityLivingBase) entityIn), AbilityFlight.class)) {
				if (abilityFlight.isEnabled()) {
					this.bipedLeftArm.rotateAngleX = 0;
					this.bipedRightArm.rotateAngleX = 0;

					this.bipedLeftLeg.rotateAngleX = 0;
					this.bipedRightLeg.rotateAngleX = 0;
				}
			}
		}
	}
}
