package lcoremodders.mods.ben10.client.model;

import lcoremodders.mods.ben10.Ben10;
import lcoremodders.mods.ben10.abilities.AbilityOmnitrix;
import lcoremodders.mods.ben10.aliens.Alien;
import lcoremodders.mods.ben10.aliens.AlienHandler;
import lucraft.mods.lucraftcore.superpowers.abilities.Ability;
import lucraft.mods.lucraftcore.util.helper.PlayerHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelBox;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;

/**
 * OmnitrixUpdated - Undefined
 * Created using Tabula 7.0.1
 */
public class ModelOmnitrix extends ModelBase {

	private static ModelOmnitrixFacePlate MODEL_FACE_PLATE = new ModelOmnitrixFacePlate();
	
	private ModelRenderer parent;
	private ModelRenderer darkgray_strap_l;
	private ModelRenderer darkgray_strap_r;
	private ModelRenderer darkgray_strap_b;
	private ModelRenderer darkgray_strap_tl;
	private ModelRenderer darkgray_strap_tr;
	private ModelRenderer outer_strap_r_1;
	private ModelRenderer outer_cornerstrap_tr_1;
	private ModelRenderer outer_strap_tr_1;
	private ModelRenderer outer_cornerstrap_br_1;
	private ModelRenderer outer_strap_r_2;
	private ModelRenderer outer_cornerstrap_tr_2;
	private ModelRenderer outer_strap_tr_2;
	private ModelRenderer outer_cornerstrap_br_2;
	private ModelRenderer outer_strap_l_1;
	private ModelRenderer outer_cornerstrap_tl_1;
	private ModelRenderer outer_strap_tl_1;
	private ModelRenderer outer_cornerstrap_bl_2;
	private ModelRenderer outer_strap_l_2;
	private ModelRenderer outer_cornerstrap_tl_2;
	private ModelRenderer outer_strap_tl_2;
	private ModelRenderer outer_cornerstrap_bl_1;
	private ModelRenderer outer_strap_f;
	private ModelRenderer outer_strap_bk;
	private ModelRenderer white_strap_l_1;
	private ModelRenderer white_strap_l_2;
	private ModelRenderer white_strap_r_1;
	private ModelRenderer white_strap_r_2;
	private ModelRenderer white_strap_b_1;
	private ModelRenderer white_strap_b_2;
	private ModelRenderer button_l;
	private ModelRenderer clock_l;
	private ModelRenderer clock_r;
	private ModelRenderer clock_f;
	private ModelRenderer clock_bk;

	public ModelOmnitrix() {
		textureWidth = 64;
		textureHeight = 32;

		parent = new ModelRenderer(this);
		parent.setRotationPoint(0.0F, 0.0F, 0.0F);

		darkgray_strap_l = new ModelRenderer(this);
		darkgray_strap_l.setRotationPoint(-2.0F, 0.0F, 0.0F);
		parent.addChild(darkgray_strap_l);
		darkgray_strap_l.cubeList.add(new ModelBox(darkgray_strap_l, 0, 22, -0.5F, -2.0F, -3.0F, 1, 4, 5, 0.0F, false));

		darkgray_strap_r = new ModelRenderer(this);
		darkgray_strap_r.setRotationPoint(2.0F, 0.0F, 0.0F);
		parent.addChild(darkgray_strap_r);
		darkgray_strap_r.cubeList.add(new ModelBox(darkgray_strap_r, 0, 22, -0.5F, -2.0F, -3.0F, 1, 4, 5, 0.0F, true));

		darkgray_strap_b = new ModelRenderer(this);
		darkgray_strap_b.setRotationPoint(0.0F, 1.5F, 0.0F);
		parent.addChild(darkgray_strap_b);
		darkgray_strap_b.cubeList.add(new ModelBox(darkgray_strap_b, 0, 26, -2.0F, 0.0F, -3.0F, 4, 1, 5, 0.0F, true));

		darkgray_strap_tl = new ModelRenderer(this);
		darkgray_strap_tl.setRotationPoint(-0.2F, -2.0F, 0.0F);
		parent.addChild(darkgray_strap_tl);
		darkgray_strap_tl.cubeList.add(new ModelBox(darkgray_strap_tl, 20, 26, -2.0F, -0.5F, -3.0F, 2, 1, 5, 0.0F, true));

		darkgray_strap_tr = new ModelRenderer(this);
		darkgray_strap_tr.setRotationPoint(0.2F, -2.0F, 0.0F);
		parent.addChild(darkgray_strap_tr);
		darkgray_strap_tr.cubeList.add(new ModelBox(darkgray_strap_tr, 20, 26, 0.0F, -0.5F, -3.0F, 2, 1, 5, 0.0F, false));

		outer_strap_r_1 = new ModelRenderer(this);
		outer_strap_r_1.setRotationPoint(2.25F, 0.0F, 2.0F);
		parent.addChild(outer_strap_r_1);
		outer_strap_r_1.cubeList.add(new ModelBox(outer_strap_r_1, 0, 21, -0.5F, -2.0F, -0.5F, 1, 4, 1, 0.0F, false));

		outer_cornerstrap_tr_1 = new ModelRenderer(this);
		outer_cornerstrap_tr_1.setRotationPoint(2.0F, -2.0F, 2.0F);
		setRotationAngle(outer_cornerstrap_tr_1, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_tr_1);
		outer_cornerstrap_tr_1.cubeList.add(new ModelBox(outer_cornerstrap_tr_1, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_strap_tr_1 = new ModelRenderer(this);
		outer_strap_tr_1.setRotationPoint(1.5F, -2.2F, 2.0F);
		parent.addChild(outer_strap_tr_1);
		outer_strap_tr_1.cubeList.add(new ModelBox(outer_strap_tr_1, 9, 22, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_cornerstrap_br_1 = new ModelRenderer(this);
		outer_cornerstrap_br_1.setRotationPoint(2.0F, 2.0F, 2.0F);
		setRotationAngle(outer_cornerstrap_br_1, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_br_1);
		outer_cornerstrap_br_1.cubeList.add(new ModelBox(outer_cornerstrap_br_1, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_strap_r_2 = new ModelRenderer(this);
		outer_strap_r_2.setRotationPoint(2.25F, 0.0F, -3.0F);
		parent.addChild(outer_strap_r_2);
		outer_strap_r_2.cubeList.add(new ModelBox(outer_strap_r_2, 0, 21, -0.5F, -2.0F, -0.5F, 1, 4, 1, 0.0F, false));

		outer_cornerstrap_tr_2 = new ModelRenderer(this);
		outer_cornerstrap_tr_2.setRotationPoint(2.0F, -2.0F, -3.0F);
		setRotationAngle(outer_cornerstrap_tr_2, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_tr_2);
		outer_cornerstrap_tr_2.cubeList.add(new ModelBox(outer_cornerstrap_tr_2, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_strap_tr_2 = new ModelRenderer(this);
		outer_strap_tr_2.setRotationPoint(1.5F, -2.2F, -3.0F);
		parent.addChild(outer_strap_tr_2);
		outer_strap_tr_2.cubeList.add(new ModelBox(outer_strap_tr_2, 9, 22, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_cornerstrap_br_2 = new ModelRenderer(this);
		outer_cornerstrap_br_2.setRotationPoint(2.0F, 2.0F, -3.0F);
		setRotationAngle(outer_cornerstrap_br_2, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_br_2);
		outer_cornerstrap_br_2.cubeList.add(new ModelBox(outer_cornerstrap_br_2, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		outer_strap_l_1 = new ModelRenderer(this);
		outer_strap_l_1.setRotationPoint(-2.25F, 0.0F, -3.0F);
		parent.addChild(outer_strap_l_1);
		outer_strap_l_1.cubeList.add(new ModelBox(outer_strap_l_1, 0, 21, -0.5F, -2.0F, -0.5F, 1, 4, 1, 0.0F, true));

		outer_cornerstrap_tl_1 = new ModelRenderer(this);
		outer_cornerstrap_tl_1.setRotationPoint(-2.0F, -2.0F, -3.0F);
		setRotationAngle(outer_cornerstrap_tl_1, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_tl_1);
		outer_cornerstrap_tl_1.cubeList.add(new ModelBox(outer_cornerstrap_tl_1, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_strap_tl_1 = new ModelRenderer(this);
		outer_strap_tl_1.setRotationPoint(-1.5F, -2.2F, -3.0F);
		parent.addChild(outer_strap_tl_1);
		outer_strap_tl_1.cubeList.add(new ModelBox(outer_strap_tl_1, 9, 22, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_cornerstrap_bl_2 = new ModelRenderer(this);
		outer_cornerstrap_bl_2.setRotationPoint(-2.0F, 2.0F, -3.0F);
		setRotationAngle(outer_cornerstrap_bl_2, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_bl_2);
		outer_cornerstrap_bl_2.cubeList.add(new ModelBox(outer_cornerstrap_bl_2, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_strap_l_2 = new ModelRenderer(this);
		outer_strap_l_2.setRotationPoint(-2.25F, 0.0F, 2.0F);
		parent.addChild(outer_strap_l_2);
		outer_strap_l_2.cubeList.add(new ModelBox(outer_strap_l_2, 0, 21, -0.5F, -2.0F, -0.5F, 1, 4, 1, 0.0F, true));

		outer_cornerstrap_tl_2 = new ModelRenderer(this);
		outer_cornerstrap_tl_2.setRotationPoint(-2.0F, -2.0F, 2.0F);
		setRotationAngle(outer_cornerstrap_tl_2, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_tl_2);
		outer_cornerstrap_tl_2.cubeList.add(new ModelBox(outer_cornerstrap_tl_2, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_strap_tl_2 = new ModelRenderer(this);
		outer_strap_tl_2.setRotationPoint(-1.5F, -2.2F, 2.0F);
		parent.addChild(outer_strap_tl_2);
		outer_strap_tl_2.cubeList.add(new ModelBox(outer_strap_tl_2, 9, 22, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_cornerstrap_bl_1 = new ModelRenderer(this);
		outer_cornerstrap_bl_1.setRotationPoint(-2.0F, 2.0F, 2.0F);
		setRotationAngle(outer_cornerstrap_bl_1, 0.0F, 0.0F, 0.7854F);
		parent.addChild(outer_cornerstrap_bl_1);
		outer_cornerstrap_bl_1.cubeList.add(new ModelBox(outer_cornerstrap_bl_1, 0, 16, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, true));

		outer_strap_f = new ModelRenderer(this);
		outer_strap_f.setRotationPoint(0.0F, 2.25F, 2.0F);
		parent.addChild(outer_strap_f);
		outer_strap_f.cubeList.add(new ModelBox(outer_strap_f, 0, 18, -2.0F, -0.55F, -0.5F, 4, 1, 1, 0.0F, true));

		outer_strap_bk = new ModelRenderer(this);
		outer_strap_bk.setRotationPoint(0.0F, 2.25F, -3.0F);
		parent.addChild(outer_strap_bk);
		outer_strap_bk.cubeList.add(new ModelBox(outer_strap_bk, 0, 18, -2.0F, -0.55F, -0.5F, 4, 1, 1, 0.0F, true));

		white_strap_l_1 = new ModelRenderer(this);
		white_strap_l_1.setRotationPoint(-2.3F, 0.0F, -2.0F);
		parent.addChild(white_strap_l_1);
		white_strap_l_1.cubeList.add(new ModelBox(white_strap_l_1, 11, 16, -0.5F, -2.2F, -0.5F, 1, 5, 1, 0.0F, true));

		white_strap_l_2 = new ModelRenderer(this);
		white_strap_l_2.setRotationPoint(-2.3F, 0.0F, 1.0F);
		parent.addChild(white_strap_l_2);
		white_strap_l_2.cubeList.add(new ModelBox(white_strap_l_2, 11, 16, -0.5F, -2.2F, -0.5F, 1, 5, 1, 0.0F, true));

		white_strap_r_1 = new ModelRenderer(this);
		white_strap_r_1.setRotationPoint(2.3F, 0.0F, 1.0F);
		parent.addChild(white_strap_r_1);
		white_strap_r_1.cubeList.add(new ModelBox(white_strap_r_1, 11, 16, -0.5F, -2.2F, -0.5F, 1, 5, 1, 0.0F, false));

		white_strap_r_2 = new ModelRenderer(this);
		white_strap_r_2.setRotationPoint(2.3F, 0.0F, -2.0F);
		parent.addChild(white_strap_r_2);
		white_strap_r_2.cubeList.add(new ModelBox(white_strap_r_2, 11, 16, -0.5F, -2.2F, -0.5F, 1, 5, 1, 0.0F, false));

		white_strap_b_1 = new ModelRenderer(this);
		white_strap_b_1.setRotationPoint(0.0F, 2.3F, 1.0F);
		parent.addChild(white_strap_b_1);
		white_strap_b_1.cubeList.add(new ModelBox(white_strap_b_1, 11, 16, -2.5F, -0.5F, -0.5F, 5, 1, 1, 0.0F, true));

		white_strap_b_2 = new ModelRenderer(this);
		white_strap_b_2.setRotationPoint(0.0F, 2.3F, -2.0F);
		parent.addChild(white_strap_b_2);
		white_strap_b_2.cubeList.add(new ModelBox(white_strap_b_2, 11, 16, -2.5F, -0.5F, -0.5F, 5, 1, 1, 0.0F, true));

		button_l = new ModelRenderer(this);
		button_l.setRotationPoint(-2.15F, -1.95F, -0.5F);
		setRotationAngle(button_l, 0.0F, 0.0F, -1.0472F);
		parent.addChild(button_l);
		button_l.cubeList.add(new ModelBox(button_l, 0, 3, -0.5F, -0.5F, -0.5F, 1, 1, 1, 0.0F, false));

		clock_l = new ModelRenderer(this);
		clock_l.setRotationPoint(-1.5F, -3.0F, -0.5F);
		setRotationAngle(clock_l, 0.0F, 0.0F, 0.7854F);
		parent.addChild(clock_l);
		clock_l.cubeList.add(new ModelBox(clock_l, 0, 7, 0.0F, 0.0F, -1.5F, 1, 1, 3, 0.0F, true));

		clock_r = new ModelRenderer(this);
		clock_r.setRotationPoint(1.5F, -3.0F, -0.5F);
		setRotationAngle(clock_r, 0.0F, 0.0F, -0.7854F);
		parent.addChild(clock_r);
		clock_r.cubeList.add(new ModelBox(clock_r, 0, 7, -1.0F, 0.0F, -1.5F, 1, 1, 3, 0.0F, false));

		clock_f = new ModelRenderer(this);
		clock_f.setRotationPoint(0.0F, -3.0F, 1.0F);
		setRotationAngle(clock_f, -0.7854F, 0.0F, 0.0F);
		parent.addChild(clock_f);
		clock_f.cubeList.add(new ModelBox(clock_f, 7, 7, -1.5F, 0.0F, 0.0F, 3, 1, 1, 0.0F, true));

		clock_bk = new ModelRenderer(this);
		clock_bk.setRotationPoint(0.0F, -3.0F, -2.0F);
		setRotationAngle(clock_bk, 0.7854F, 0.0F, 0.0F);
		parent.addChild(clock_bk);
		clock_bk.cubeList.add(new ModelBox(clock_bk, 7, 7, -1.5F, 0.0F, -1.0F, 3, 1, 1, 0.0F, true));
	}

	public void render(Entity entityIn, float scale) {
		if (entityIn instanceof EntityPlayer) {
			for (AbilityOmnitrix abilityOmnitrix : Ability.getAbilitiesFromClass(Ability.getAbilities((EntityLivingBase) entityIn), AbilityOmnitrix.class)) {
				if (PlayerHelper.hasSmallArms((EntityPlayer) entityIn)) {
					GlStateManager.translate(this.parent.offsetX, this.parent.offsetY, this.parent.offsetZ);
					GlStateManager.translate(this.parent.rotationPointX * scale, this.parent.rotationPointY * scale, this.parent.rotationPointZ * scale);
					GlStateManager.scale(0.9D, 0.9D, 0.9D);
					GlStateManager.translate(-this.parent.offsetX, -this.parent.offsetY, -this.parent.offsetZ);
					GlStateManager.translate(-this.parent.rotationPointX * scale, -this.parent.rotationPointY * scale, -this.parent.rotationPointZ * scale);
					render((EntityPlayer) entityIn, 0.0625F, abilityOmnitrix);
				} else
					render((EntityPlayer) entityIn, 0.0625F, abilityOmnitrix);
				
				break;
			}
		}
	}

	private static final ResourceLocation MAIN_TEX = new ResourceLocation(Ben10.MODID, "textures/models/omnitrix/main.png");
	public void render(EntityPlayer player, float scale, AbilityOmnitrix omnitrix) {
		Minecraft mc = Minecraft.getMinecraft();
		
		boolean isSelectingAlien = omnitrix.getDataManager().get(AbilityOmnitrix.IS_SELECTING_ALIEN);
		Alien alien = AlienHandler.getAlien(player);

		mc.getTextureManager().bindTexture(MAIN_TEX);
		this.parent.render(scale);
		if (isSelectingAlien) {
			if (alien != null) {
				mc.getTextureManager().bindTexture(new ResourceLocation(Ben10.MODID, "textures/models/omnitrix/alien_icons/alien_" + alien.getName() + ".png"));
			}
		} else {
			mc.getTextureManager().bindTexture(new ResourceLocation(Ben10.MODID, "textures/models/omnitrix/not_selecting.png"));
		}
		MODEL_FACE_PLATE.render(scale);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
